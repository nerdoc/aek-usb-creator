/*
 * Diese Datei ist Teil von AEK-USB-Creator.
 * AEK-USB-Creator ist Freie Software: Sie können es unter den Bedingungen
 *
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * AEK-USB-Creator wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include <QDir>
#include <QDebug>

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    connect(ui->buttonChoosePath, &QPushButton::clicked, this, &SettingsDialog::openPathDlg);
    connect(ui->lineEditZipPath, &QLineEdit::textChanged, this, &SettingsDialog::updateVersions);
}



SettingsDialog::~SettingsDialog()
{
    delete ui;
    delete pathDlg;
}

void SettingsDialog::setZipPath(QString path)
{
    ui->lineEditZipPath->setText(path);
    updateVersions();
}

QString SettingsDialog::zipPath()
{
    return ui->lineEditZipPath->text();
}

void SettingsDialog::setDefaultVersion(QString version)
{
    int number = ui->comboVersions->count();
    if (number > 0) {
        if (!version.isEmpty()) {
            ui->comboVersions->setCurrentIndex(ui->comboVersions->findText(version));
        } else {
            ui->comboVersions->setCurrentIndex(0);
        }
    }
}

QString SettingsDialog::defaultVersion()
{
    return ui->comboVersions->currentText();
}


/**
 * @brief SettingsDialog::getVersionList
 *
 * This function searches for usbcard-*.zip files in the given path. It then extracts the
 * version names from the filename.
 * @param path directory to search in.
 * @return a QStringList that contains the version strings of the found zip files
 */
QStringList SettingsDialog::getVersionList(QString path) {

    QDir dir(path);
    QStringList files, nameFilter, versionList;
    nameFilter << "usbcard-*.zip";
    files = dir.entryList(nameFilter, QDir::Files | QDir::NoSymLinks);

    foreach(QString filename, files) {
        if (filename.endsWith(".zip")) {
            versionList.append(filename.left(filename.length()-4) // remove ".zip"
                    .remove(0,8)); // remove "usbcard-"

        }
    }
    return versionList;
}

void SettingsDialog::updateVersions()
{

    QStringList versionList = getVersionList(ui->lineEditZipPath->text());

    //TODO: sort versions!


    ui->comboVersions->clear();

    if (versionList.isEmpty()) {
        ui->labelMessage->setText("In diesem Verzeichnis wurden leider keine gezippten USB-Card-Dateien gefunden.");
        ui->lineEditZipPath->setStyleSheet("QLineEdit { color: red;}");
    } else {
        ui->labelMessage->setText("");
        ui->comboVersions->addItems(versionList);
        ui->lineEditZipPath->setStyleSheet("");
    }
}

void SettingsDialog::openPathDlg()
{
    if (! pathDlg) {
        pathDlg = new QFileDialog(this, "Pfad auswählen", ui->lineEditZipPath->text());
        pathDlg->setFileMode(QFileDialog::DirectoryOnly);
    }
    if (pathDlg->exec() == QDialog::Accepted) {
        QString directory = pathDlg->selectedFiles().first();
        ui->lineEditZipPath->setText(directory);
        updateVersions();
    }
}
