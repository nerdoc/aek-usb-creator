/*
 * Diese Datei ist Teil von AEK-USB-Creator.
 * AEK-USB-Creator ist Freie Software: Sie können es unter den Bedingungen
 *
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * AEK-USB-Creator wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <qfiledialog.h>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

    void setZipPath(QString path);
    QString zipPath();

    void setDefaultVersion(QString version);
    QString defaultVersion();


public slots:
    void updateVersions();


private:
    QStringList getVersionList(QString path);
    void openPathDlg();

    Ui::SettingsDialog *ui;
    QFileDialog * pathDlg = 0;
};

#endif // SETTINGSDIALOG_H
