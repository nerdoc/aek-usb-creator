/*
 * Diese Datei ist Teil von AEK-USB-Creator.
 * AEK-USB-Creator ist Freie Software: Sie können es unter den Bedingungen
 *
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * AEK-USB-Creator wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

#include "usbdevicelist.h"

#include <QDir>
#include <QDebug>

USBDeviceList::USBDeviceList(QObject *parent) : QObject(parent)
{
}

USBDeviceList::~USBDeviceList()
{
}

QList<QStorageInfo> USBDeviceList::getDevices()
{
    QList<QStorageInfo> info;
    foreach (const QStorageInfo &storage, QStorageInfo::mountedVolumes()) {
        if (storage.isValid() && storage.isReady())
            //TODO: properly detect which device is an USB stick.
            // vfat is very unreliable...
            if (storage.fileSystemType() == "vfat") {
                if (!storage.isReadOnly()) {
                    qDebug() << storage.rootPath() << storage.fileSystemType();
                    info.append(storage);
                }
            }
    }
    return info;
}

