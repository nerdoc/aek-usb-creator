#include "aboutdialog.h"
#include "ui_aboutdialog.h"

#include "appversion.h"
#include <QDate>

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    int firstYear = 2015;
    int thisYear = QDate::currentDate().year();
    QString year;
    if (firstYear == thisYear)
        year = QString::number(firstYear);
    else
        year = QString::number(firstYear) + "-" +  QString::number(thisYear);

    ui->labelCopyright->setText(QString("Copyright %1 Dr. Christian González").arg(year));
    ui->labelVersion->setText(QString("Version %1").arg(APPVERSION));
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
