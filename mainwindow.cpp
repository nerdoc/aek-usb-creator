/*
 * Diese Datei ist Teil von AEK-USB-Creator.
 * AEK-USB-Creator ist Freie Software: Sie können es unter den Bedingungen
 *
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * AEK-USB-Creator wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "usbcardzippackage.h"
#include "appversion.h"
#include "aboutdialog.h"

#include <QSettings>
#include <QStorageInfo>
#include <QDebug>
#include <QMessageBox>

#include <QTime>

void zip(QString filename , QString zipfilename);
void unzip(QString zipfilename , QString filename);

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qsrand(QTime::currentTime().msec());
    QSettings settings("Ärztekammer Salzburg", "USBCreator");//TODO: USBCreator

    setWindowIcon(QIcon(":/images/about-logo.png"));
    setWindowTitle("USB-Card-Creator der Ärztekammer Salzburg");
    ui->setupUi(this);

    zipfilePath = settings.value("zipfile_path", "").toString();
    defaultVersion = settings.value("zipfile_version", "").toString();

    settingsDlg = new SettingsDialog(this);

    updateUsbDevices();
    updateButtonStatus();

    connect(ui->actionSettings, &QAction::triggered, this, &MainWindow::openSettingsDialog);
    connect(ui->generatePasswordButton, SIGNAL(clicked()), this, SLOT(generatePassword()));
    connect(settingsDlg, &SettingsDialog::accepted, this, &MainWindow::saveSettings);
    connect(ui->buttonStart, &QPushButton::clicked, this, &MainWindow::copyToUsbCard);
    connect(ui->buttonRefreshDevices, &QPushButton::clicked, this, &MainWindow::updateUsbDevices);
    connect(ui->passwordEdit, &QLineEdit::textChanged, this, &MainWindow::updateButtonStatus);
    connect(ui->usernameEdit, &QLineEdit::textChanged, this, &MainWindow::updateButtonStatus);
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::showAbout);
    connect(ui->actionAboutQt, &QAction::triggered, this, &MainWindow::showAboutQt);

    ui->usernameEdit->setFocus();
    generatePassword();
}



MainWindow::~MainWindow()
{
    delete ui;
    delete settingsDlg;
}

void MainWindow::statusLog(QString message)
{
    statusBar()->showMessage(message);
}

void MainWindow::copyToUsbCard()
{
    QString devicePath = ui->comboUSB->currentData().toString();
    qDebug() << "devicePath: " << devicePath;
    if (devicePath.isEmpty()) {
        statusLog("'"+ devicePath +"' ist kein gültiger Pfad zu einer USB-Card.");
        return;
    }

    // create a new QStorageInfo again based on the selection
    // this should be the easiest way to find it again.
    QStorageInfo storage = QStorageInfo(devicePath);
    if (storage.isValid() && storage.isReady()) {

        // start copying!
        QString version = settings.value("default_version","").toString();
        if (version.isEmpty()) {
            statusLog("Fehler: eingestellte USB-Card-Version ist falsch.");
            return;
        }

        UsbCardZipPackage srcFile(settings.value("zipfile_path").toString().append("/usbcard-" + version + ".zip"));

        ui->buttonStart->setEnabled(false);
        statusLog("Kopiere Daten auf USB-Card...");
        srcFile.unzipTo(storage.rootPath());

        // copy credentials
        QFile credentialsFile(storage.rootPath().append("/data/credentials.js"));
        if (!credentialsFile.open(QFile::WriteOnly)) {
            statusLog("Fehler beim Schreiben von Benutzername/Passwort!");
            ui->buttonStart->setEnabled(true);
            return ;
        }
        credentialsFile.write(QString("var username = '%1';\n").arg(ui->usernameEdit->text()).toLatin1());
        credentialsFile.write(QString("var password = '%1';\n").arg(ui->passwordEdit->text()).toLatin1());
        credentialsFile.close();
        statusLog("Fertig.");
        QMessageBox::information(this, "Fertig", "Das Schreiben der USB-Card wurde beendet.\n"
                                                 "Bitte werfen Sie das Laufwerk noch manuell aus, bevor sie die Card aus dem USB-Slot ziehen, "
                                                 "um Datenverlust zu vermeiden.");
        ui->buttonStart->setEnabled(true);
        ui->usernameEdit->clear();
        generatePassword();

    } else {
        statusLog("Fehler beim Kopieren.");
    }

}

QString randString(int len)
{
    QString str;
    const QString availableChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    str.resize(len);
    for (int s = 0; s < len ; ++s)
        str[s] = availableChars[qrand() % availableChars.length()];

    return str;
}

void MainWindow::generatePassword()
{
    int pw_length = settings.value("password_length", 8).toInt();
    ui->passwordEdit->setText(randString(pw_length));
}

void MainWindow::openSettingsDialog()
{
    settingsDlg->setZipPath(settings.value("zipfile_path", "").toString());
    settingsDlg->setDefaultVersion(settings.value("default_version", "").toString());
    if (settingsDlg->exec() == QDialog::Accepted) {
        settings.setValue("zipfile_path", settingsDlg->zipPath());
        settings.setValue("default_version", settingsDlg->defaultVersion());
    }
}

void MainWindow::saveSettings()
{
    settings.setValue("zipfile_path", settingsDlg->zipPath());
}

void MainWindow::updateUsbDevices()
{
    devicesList = USBDeviceList::getDevices();
    ui->comboUSB->clear();
    if (devicesList.isEmpty()) {
        ui->comboUSB->addItem("Kein USB-Gerät gefunden");
        ui->comboUSB->setEnabled(false);
    } else {
        foreach(const QStorageInfo &storage, devicesList) {
            ui->comboUSB->addItem(storage.device(),storage.rootPath());
        }
        ui->comboUSB->setEnabled(true);
    }
}

void MainWindow::updateButtonStatus()
{
    ui->buttonStart->setEnabled(
                !ui->passwordEdit->text().isEmpty() &&
                !ui->usernameEdit->text().isEmpty()
                );
}

void MainWindow::showAbout()
{
    AboutDialog *aboutDlg = new AboutDialog;
    aboutDlg->exec();
    delete aboutDlg;
}

void MainWindow::showAboutQt() {
    QMessageBox::aboutQt(this, "Über Qt");
}

