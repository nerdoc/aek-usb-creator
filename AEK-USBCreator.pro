
QT      += core gui widgets

TARGET = AEK-USBCreator
TEMPLATE = app

include(contrib/quazip/quazip/quazip.pri)

SOURCES += main.cpp\
        mainwindow.cpp \
    settingsdialog.cpp \
    usbdevicelist.cpp \
    usbcardzippackage.cpp \
    aboutdialog.cpp

HEADERS  += mainwindow.h \
    settingsdialog.h \
    usbdevicelist.h \
    usbcardzippackage.h \
    appversion.h \
    aboutdialog.h

FORMS    += mainwindow.ui \
    settingsdialog.ui \
    aboutdialog.ui

RESOURCES += \
    resources.qrc

DISTFILES += \
    COPYING
