/*
 * Diese Datei ist Teil von AEK-USB-Creator.
 * AEK-USB-Creator ist Freie Software: Sie können es unter den Bedingungen
 *
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * AEK-USB-Creator wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "settingsdialog.h"
#include "usbdevicelist.h"

#include <QMainWindow>
#include <QSettings>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void statusLog(QString message);
    void copyToUsbCard();

public slots:
    void generatePassword();
    void openSettingsDialog();
    void saveSettings();

    void updateUsbDevices();
    void updateButtonStatus();
    void showAbout();
    void showAboutQt();

private:
    Ui::MainWindow *ui;
    SettingsDialog *settingsDlg;
    QSettings settings;
    QList<QStorageInfo> devicesList;

    QString zipfilePath;
    QString defaultVersion;
};

#endif // MAINWINDOW_H
