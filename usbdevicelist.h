/*
 * Diese Datei ist Teil von AEK-USB-Creator.
 * AEK-USB-Creator ist Freie Software: Sie können es unter den Bedingungen
 *
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * AEK-USB-Creator wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

#ifndef USBDEVICELIST_H
#define USBDEVICELIST_H

#include <QObject>
#include <QStorageInfo>

class USBDeviceList : public QObject
{
    Q_OBJECT
public:
    explicit USBDeviceList(QObject *parent = 0);
    ~USBDeviceList();

    static QList<QStorageInfo> getDevices();

signals:

public slots:
private:
    static QList<QStorageInfo> mountedDevices;
};

#endif // USBDEVICELIST_H
