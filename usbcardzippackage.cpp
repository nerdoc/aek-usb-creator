/*
 * Diese Datei ist Teil von AEK-USB-Creator.
 * AEK-USB-Creator ist Freie Software: Sie können es unter den Bedingungen
 *
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 * veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 * AEK-USB-Creator wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

#include "usbcardzippackage.h"
#include <QDebug>
#include <QDir>
#include <JlCompress.h>

UsbCardZipPackage::UsbCardZipPackage(const QString &path, QObject *parent) :
    QObject(parent), mVersion(""), mPath(path)
{
    QString tmp = "";
    if (path.endsWith(".zip")) {
        // remove trailing ".zip"
        tmp = path.left(path.length()-4);

        int i = path.lastIndexOf("/usbcard-");
        if (i > -1) {
            // remove "$path/usbcard-" (9 is length of '/usbcard-')
            tmp = tmp.right(tmp.length() - i - 9);
        }

        qDebug() << tmp;
        mVersion = tmp;
    }
}

bool UsbCardZipPackage::unzipTo(QString destPath)
{
    // check if zip file is a valid file and at an accessible path
    if (mPath.isEmpty() || !QDir(mPath).isReadable()) {
        qDebug() << "UsbCardZipPackage: Error: no/unaccessable zipfile specified: " << mPath;
        return false;
    }

    // check if destination is writeable
    if (!QFileInfo(destPath).isWritable()) {
        qDebug() << "Error: destination path '" << destPath << "'' is not writeable.";
        return false;
    }
    qDebug() << mPath << " --> " << destPath;

    JlCompress::extractDir(mPath, destPath);

    return true;
}

QString UsbCardZipPackage::version()
{
    return mVersion;
}

